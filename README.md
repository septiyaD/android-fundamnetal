# Android Fundamental
Dalam project modul android fundamental ini akan dibahas dari mulai basic pengenalan dasar android hingga pembuatan simple project, dalam hal ini modul didasarkan pada pembelajaran 1 semester dalam pembelajaran SMK RPL kelas 10 (X) Rabbaanii, Berikut adalah ulasannya:

Pada pembukaan modul pembahasan Android Fundamental ini saya akan memaparkan beberapa teori dasar untuk memahami konsep dasar pengembangan aplikasi mobile khususnya Android, dan dibuka dengan materi pertama yaitu:

### [Pengenalan Android](https://gitlab.com/septiyaD/android-fundamnetal/tree/master/Pengenalan%20Android)
------------------------------------------------------------------
> Secara Garis Besar Pengenalan ini akan dibahas tentang bagaimana Android bisa Merajai pasar hingga sekarang, padahal dulunya pencipta dari Sistem operasi Mobile ini ditujukan untuk membuat sebuah device untuk kamera, tetapi dengan melesatnya perkembangan teknologi mobile, pencipta dengan tim akhirnya memutuskan untuk membangun Sistem Operasi Mobile hingga sekarang yang kita kenal hingga sekarang, dengan logo Maskotnya yaitu gambar seperti dibawah

